package az.unibank.unitech.dto;

import az.unibank.unitech.enums.Currency;
import lombok.Data;

@Data
public class AccountDto {
    private Long id;
    private String accountNumber;
    private Currency currency;
    private Double balance;
}
