package az.unibank.unitech.repository;

import az.unibank.unitech.entitty.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

  Optional<Account> findAccountByAccountNumberAndIsActiveTrue(String accountNumber);


}
