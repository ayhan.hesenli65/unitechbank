package az.unibank.unitech.jwt.service;

import az.unibank.unitech.jwt.dao.RoleDao;
import az.unibank.unitech.jwt.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    private RoleDao roleDao;

    public Role createNewRole(Role role) {
        return roleDao.save(role);
    }
}
