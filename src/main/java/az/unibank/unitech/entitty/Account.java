package az.unibank.unitech.entitty;

import az.unibank.unitech.enums.Currency;
import az.unibank.unitech.jwt.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "accounts")
@Data
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;
    private Double balance;
    @Column(name = "account_number" ,unique = true)
    private String accountNumber;
    @Enumerated(value = EnumType.STRING)
    private Currency currency;
    private Boolean isActive;

    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    @JsonIgnore
    User user;


}
