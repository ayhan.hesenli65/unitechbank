package az.unibank.unitech.entitty;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
public class Transaction {


    private String accountNumberfrom;
    private String accountNumberTo;
    private String currencyFrom;
    private String currencyTo;
    private Double amount;
    private LocalDateTime time;
}
