package az.unibank.unitech.controller;

import az.unibank.unitech.dto.AccountDto;
import az.unibank.unitech.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {
    private final AccountService accountService;

    @GetMapping
    public ResponseEntity<List<AccountDto>> findAll(){
        return ResponseEntity.ok(accountService.findAll());
    }

    @GetMapping("/{id}")
    public  ResponseEntity<AccountDto> findByid(@PathVariable("id") Long id){
        return ResponseEntity.ok(accountService.findByid(id));
    }

    @PostMapping
    public  ResponseEntity<AccountDto> save(@RequestBody AccountDto accountDto){

        return ResponseEntity.ok(accountService.save(accountDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<AccountDto> update(@PathVariable("id") Long id,@RequestBody AccountDto accountDto){
        return ResponseEntity.ok(accountService.update(id,accountDto));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id){
        accountService.delete(id);
    }






}
