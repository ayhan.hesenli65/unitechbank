package az.unibank.unitech.controller;

import az.unibank.unitech.dto.AccountDto;
import az.unibank.unitech.entitty.Account;
import az.unibank.unitech.entitty.Transaction;
import az.unibank.unitech.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sendMoney")
@RequiredArgsConstructor
public class TransactionController {

    private final AccountService accountService;
    @PostMapping()
    public ResponseEntity<Account> sendTransfer( @RequestBody Transaction transaction){
    return  ResponseEntity.ok(   accountService.sendMoney(transaction));
    }

}
