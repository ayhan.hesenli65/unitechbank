package az.unibank.unitech.service;

import az.unibank.unitech.dto.AccountDto;
import az.unibank.unitech.entitty.Account;
import az.unibank.unitech.entitty.Transaction;
import az.unibank.unitech.enums.Currency;
import az.unibank.unitech.exception.GlobalExceptionHandler;
import az.unibank.unitech.exception.NotFoundException;
import az.unibank.unitech.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final ModelMapper modelMapper;


    public List<AccountDto> findAll() {
        return accountRepository.findAll().stream().map(
                        account -> modelMapper.map(account, AccountDto.class))
                .collect(Collectors.toList());
    }

    public AccountDto findByid(Long id) {
        Account itEquipment = accountRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("account not found with id - %s", id)
        ));
        return modelMapper.map(accountRepository.save(itEquipment), AccountDto.class);

    }

    public AccountDto save(AccountDto accountDto) {
        Account account = modelMapper.map(accountDto, Account.class);

        return modelMapper.map(accountRepository.save(account), AccountDto.class);
    }

    public void delete(Long id) {
        Account account = accountRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("account not found with id - %s", id)
        ));
        accountRepository.delete(account);
    }

    public AccountDto update(Long id, AccountDto accountDto) {
        Account account = accountRepository.findById(id).orElseThrow(() -> new NotFoundException(
                String.format("account not found with id - %s", id)
        ));
        modelMapper.map(accountDto, account, "map");

        account.setId(id);
        return modelMapper.map(accountRepository.save(account), AccountDto.class);
    }

    @Transactional
    public Account sendMoney(Transaction transaction) {
        ;
        Account accountFrom = accountRepository.findAccountByAccountNumberAndIsActiveTrue(transaction.getAccountNumberfrom()).orElseThrow(() -> new NotFoundException(
                String.format("Accountnumber not found with id - %s", transaction.getAccountNumberfrom())
        ));
        ;


        Account accountTo = accountRepository.findAccountByAccountNumberAndIsActiveTrue(transaction.getAccountNumberTo()).orElseThrow(() -> new NotFoundException(
                String.format("Accountnumber not found with number - %s", transaction.getAccountNumberfrom())));


        if (accountTo.getAccountNumber().equals(transaction.getAccountNumberTo())
                && accountFrom.getBalance() >= transaction.getAmount()) {


            Double balance = accountFrom.getBalance() - transaction.getAmount();
            accountFrom.setBalance(balance);
            accountRepository.save(accountFrom);
            //AZN+ DOLLAR
            if (transaction.getCurrencyFrom().equals("AZN") && transaction.getCurrencyTo().equals("DOLLAR")
                    && accountFrom.getCurrency().equals(Currency.AZN) && accountTo.getCurrency().equals(Currency.DOLLAR)) {

                Double balance1 = accountTo.getBalance() + (transaction.getAmount() / 1.7);
                accountTo.setBalance(balance1);
                accountRepository.save(accountTo);
                return accountFrom;
            }
            if (transaction.getCurrencyFrom().equals("DOLLAR") && transaction.getCurrencyTo().equals("AZN")
                    && accountFrom.getCurrency().equals(Currency.DOLLAR) && accountTo.getCurrency().equals(Currency.AZN)) {

                Double balance1 = accountTo.getBalance() + (transaction.getAmount() * 1.7);
                accountTo.setBalance(balance1);
                accountRepository.save(accountTo);
                return accountTo;
            }
            //AZN + EURO
            if (transaction.getCurrencyFrom().equals("AZN") && transaction.getCurrencyTo().equals("EURO")
                    && accountFrom.getCurrency().equals(Currency.AZN) && accountTo.getCurrency().equals(Currency.EURO)) {

                Double balance1 = accountTo.getBalance() + (transaction.getAmount() / 2);
                accountTo.setBalance(balance1);
                accountRepository.save(accountTo);
                return accountFrom;
            }

            if (transaction.getCurrencyFrom().equals("EURO") && transaction.getCurrencyTo().equals("AZN")
                    && accountFrom.getCurrency().equals(Currency.EURO) && accountTo.getCurrency().equals(Currency.AZN)) {

                Double balance1 = accountTo.getBalance() + (transaction.getAmount() * 2);
                accountTo.setBalance(balance1);
                accountRepository.save(accountTo);
                return accountTo;
            }
//EORU DOLLAR
            if (transaction.getCurrencyFrom().equals("EURO") && transaction.getCurrencyTo().equals("DOLLAR")
                    && accountFrom.getCurrency().equals(Currency.EURO) && accountTo.getCurrency().equals(Currency.DOLLAR)) {

                Double balance1 = accountTo.getBalance() + (transaction.getAmount() * 1.1);
                accountTo.setBalance(balance1);
                accountRepository.save(accountTo);
                return accountTo;
            }

            if (transaction.getCurrencyFrom().equals("DOLLAR") && transaction.getCurrencyTo().equals("EURO")
                    && accountFrom.getCurrency().equals(Currency.DOLLAR) && accountTo.getCurrency().equals(Currency.EURO)) {

                Double balance1 = accountTo.getBalance() + (transaction.getAmount() / 1.1);
                accountTo.setBalance(balance1);
                accountRepository.save(accountTo);
                return accountTo;
            }
        } else {
            return accountFrom;
        }
        return accountTo;
    }


}
